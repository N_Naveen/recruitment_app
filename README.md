# Basic Recruitment Application

##### Requirement Packages:
- python 2.7
- django 1.11.3

###### Clone the repository

> git clone https://N_Naveen@bitbucket.org/N_Naveen/recruitment_app.git

###### Goto project folder
> cd recruitment_app

Used Sqlite database
User already loaded


###### Run the Project
> python manage.py runserver

Open Browser, Goto

http://127.0.0.1:8000/


Open Application form, any guest user can submit application


For Admin access, click login button on header
##### Login details
- username : root
- password : secret123


After login, you can see the list of applications received.

From there, you can approve or reject the applications



You can logout, by clicking Logout button on header


