"""recruitment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
import django.views.static as django_static_view

from recruit_app import views

urlpatterns = [

	# Media URL's
    url(r'^site_media/(?P<path>.*)$', django_static_view.serve, {'document_root': settings.MEDIA_ROOT }),
    url(r'^static/admin/(?P<path>.*)$', django_static_view.serve, {'document_root': settings.STATIC_ROOT }),

    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home, name="home"),
    url(r'^confirmation/$', views.confirmation, name="confirmation"),
    url(r'^signin/$', views.signin, name="signin"),
    url(r'^signout/$', views.signout, name="signout"),
    url(r'^applications/list/$', views.admin_recruit_application_list, name="admin_recruit_application_list"),
    url(r'^application/update/status/$', views.admin_recruit_application_update_status, name="admin_recruit_application_update_status")
    
    
]
