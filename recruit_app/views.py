# -*- coding: utf-8 -*-

# From python
import sys
import json


# From django
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect, HttpResponse

from app_forms import *
import recruit_api

login_url = settings.LOGIN_URL


def _fn():
    '''
    returns current function name
    '''
    func_name = sys._getframe(1).f_code.co_name
    return func_name


def home(request):
    fn = _fn()
    template_name = fn+".html"
    try:
        form = RecruitApplicationForm()

        if request.method == "POST":
            form = RecruitApplicationForm(request.POST)
            if form.is_valid():
                result = recruit_api.save_application(request, form)
                return HttpResponseRedirect(reverse('confirmation'))

    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return render(request, template_name, locals())


def confirmation(request):
    fn = _fn()
    template_name = fn+".html"
    try:
        pass
    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return render(request, template_name, locals())



def signin(request):
    fn = _fn()
    template_name = fn+".html"
    try:
        form = SigninForm()
        if request.method == "POST":
            form = SigninForm(request.POST)
            if form.is_valid():
                result = recruit_api.signin(request, request.POST.copy())
                return HttpResponseRedirect(reverse('admin_recruit_application_list'))
    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return render(request, template_name, locals())


def signout(request):
    try:
        recruit_api.signout(request)
    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return HttpResponseRedirect(reverse('home'))


@login_required(login_url=login_url)
@user_passes_test(lambda u: u.is_superuser)
def admin_recruit_application_list(request):
    fn = _fn()
    template_name = fn+".html"
    try:
        result, recruit_applications = recruit_api.list_all_applications(request)
    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return render(request, template_name, locals())


@login_required(login_url=login_url)
@user_passes_test(lambda u: u.is_superuser)
def admin_recruit_application_update_status(request):
    try:
        result, recruit_obj = recruit_api.update_application_status(request, request.POST.copy())
    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return HttpResponse(json.dumps(recruit_obj))
