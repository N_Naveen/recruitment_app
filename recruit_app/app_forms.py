# -*- coding: utf-8 -*-

import re
from django.forms.widgets import *
from django import forms
from django.contrib.auth.models import User

from models import *


class SigninForm(forms.Form):
    username    = forms.CharField(label="User Name / Email Address", max_length=50, required=True, widget=forms.TextInput())
    password = forms.CharField(label="Password", max_length=30, required=True, widget=forms.PasswordInput())
    
    def clean(self):
        username = self.cleaned_data.get("username",None)
        password = self.cleaned_data.get("password", None)
        if username and password:
            user = User.objects.filter(email__iexact=username).first()
            if not user:
                user = User.objects.filter(username__iexact=username).first()
            
            if not user:
                raise forms.ValidationError("There is no account in our system for this Username")    
            
            if not user.check_password(password):
                raise forms.ValidationError("Invalid Password")
            
        return self.cleaned_data


class RecruitApplicationForm(forms.ModelForm):
    
    class Meta:
        model = RecruitApplication
        fields = ('name','email','phone','years_of_experience')

    def clean_phone(self):

        phone = self.cleaned_data["phone"]
        phone_validator=re.compile(r'^(\d{10})(?:\s|$)')
        if not phone_validator.match(phone):
            raise forms.ValidationError('Please enter a valid phone number')
        return phone
