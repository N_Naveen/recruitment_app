# -*- coding: utf-8 -*-

# From python
import sys

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from models import *

def _fn():
    '''
    returns current function name
    '''
    func_name = sys._getframe(1).f_code.co_name
    return func_name


def signin(request, p_dict):
    result = False
    try:
    	username = p_dict['username'].strip()
        password = p_dict['password'].strip()

        user = User.objects.filter(email__iexact=username).first()
        if not user:
            user = User.objects.get(username__iexact=username)

        #Authenticate the user
        user = authenticate(username=user.username, password=password)

        #if user exists
        if user is not None:
            login(request, user)
            result = True

    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return result


def signout(request):
    result = False
    try:
    	logout(request)

    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return result


def save_application(request, form):
    try:
        recruit_application = form.save()

    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return recruit_application


def list_all_applications(request):
    result = False
    try:
    	recruit_applications = RecruitApplication.objects.all().order_by("-created_on")
        result = True
    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return result, recruit_applications


def update_application_status(request, p_dict):
    result = False
    recruit_obj = dict()
    try:
        recruit_obj['result'] = False
        application_id = p_dict.get("application_id", None)
        status = p_dict.get("status", None)

        recruit_application = RecruitApplication.objects.filter(id=application_id).first()
        if recruit_application:
            recruit_application.status = status.upper()
            recruit_application.save()

            recruit_obj['response_text'] = recruit_application.get_status_display()
            recruit_obj['result'] = True

    	result = True
    except Exception, e:
        print "Error at line no :%s, %s"%(sys.exc_traceback.tb_lineno, e)
    return result, recruit_obj

