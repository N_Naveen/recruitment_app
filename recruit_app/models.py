# -*- coding: utf-8 -*-

from django.db import models

APPLICATION_STATUS = (('PENDING','Pending'),('APPROVED','Approved'),('REJECTED','Rejected'))

class RecruitApplication(models.Model):
    name = models.CharField(max_length=140)
    phone = models.CharField(max_length=10)
    email = models.EmailField()
    years_of_experience = models.IntegerField(default=0)
    status = models.CharField(max_length=10, default='PENDING', choices=APPLICATION_STATUS)
    created_on     = models.DateTimeField(auto_now_add=True)
    updated_on     = models.DateTimeField(auto_now=True)
       
    def __unicode__(self):
        return "%s" %(self.name)